import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/dashboard",
      component: () => import("@/view/layout/Layout"),
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: () => import("@/view/pages/Dashboard.vue")
        }, 
        // { 
        // name: "manifest",
        // path: "/manifest",
        // component: () => import("@/view/pages/Manifest")
        // },
        // { 
        // name: "createmanifest",
        // path: "/createmanifest",
        // component: () => import("@/view/pages/CreateManifest")
        // },
        // { 
        //   name: "updatemanifest",
        //   path: "/updatemanifest",
        //   component: () => import("@/view/pages/UpdateManifest")
        // },

        // Manifest Module
        { 
          name: "manifest-index",
          path: "/manifests/list",
          component: () => import("@/view/pages/Manifest/manifestIndex")
        },
        { 
          name: "manifest-detail",
          path: "/manifests/view/:id",
          component: () => import("@/view/pages/Manifest/manifestView")
        },

        // My Profile Module
        { 
          name: "profileOverview",
          path: "/profile",
          component: () => import("@/view/pages/MyProfile/ProfileOverview.vue")
        },
        
        { 
          name: "profileInfo",
          path: "/profile/Info",
          component: () => import("@/view/pages/MyProfile/PersonalInfo.vue")
        },

        { 
          name: "updatepassword",
          path: "/profile/updatepassword",
          component: () => import("@/view/pages/MyProfile/UpdatePassword.vue")
        },
        { 
          name: "accountInfo",
          path: "/profile/accountInfo",
          component: () => import("@/view/pages/MyProfile/AccountInfo.vue")
        },
        { 
          name: "user-preferences",
          path: "/profile/preferences",
          component: () => import("@/view/pages/MyProfile/Preferences.vue")
        },

        { 
          name: "users",
          path: "/usersList",
          component: () => import("@/view/pages/Users.vue")
        },
        { 
          name: "add-user",
          path: "/userList/add-user",
          component: () => import("@/view/pages/addUser")
        },

        { 
          name: "addclient",
          path: "/user/addclient",
          component: () => import("@/view/pages/addClient")
        },
        
        // Order Module
        { 
          name: "consignments-listing",
          path: "/consignments/view",
          component: () => import("@/view/pages/Consignments/consignmentIndex")
        },
        { 
          name: "consignments-pricing",
          path: "/consignments/pricing",
          component: () => import("@/view/pages/Consignments/checkConsignmentPricing")
        },

        // Product Module
        { 
          name: "product",
          path: "/products/view",
          component: () => import("@/view/pages/ProductModule/Index")
        },
        { 
          name: "createProduct",
          path: "/products/create",
          component: () => import("@/view/pages/ProductModule/CreateProduct")
        },
        { 
          name: "editProduct",
          path: "products/edit/:e_id",
          component: () => import("@/view/pages/ProductModule/edit")
        },
        { 
          name: "viewProduct",
          path: "products/view/:id",
          component: () => import("@/view/pages/ProductModule/view")
        },

        // Client Module
        { 
          name: "CreateClient",
          path: "/client/create",
          component: () => import("@/view/pages/ClientModule/Create")
        },
        { 
          name: "ClientIndex",
          path: "/clients",
          component: () => import("@/view/pages/ClientModule/Index")
        },
        { 
          name: "viewClient",
          path: "/view-client/:id",
          component: () => import("@/view/pages/ClientModule/view")
        },
        { 
          name: "editClient",
          path: "/edit-client/:e_id",
          component: () => import("@/view/pages/ClientModule/edit")
        },

        // Client Card
        { 
          name: "client-detail",
          path: "/client/detail",
          component: () => import("@/view/pages/ClientProfile/ClientDetail")
        },
        // { 
        //   name: "client-addcard",
        //   path: "/client/add-card",
        //   component: () => import("@/view/pages/ClientProfile/AddCard")
        // },
        // { 
        //   name: "client-cards",
        //   path: "/client/cards",
        //   component: () => import("@/view/pages/ClientProfile/ClientCards")
        // },
        { 
          name: "client-users",
          path: "/client/users",
          component: () => import("@/view/pages/ClientProfile/ClientUsers")
        },


        { 
          name: "viewClientCards",
          path: "/clientcards/view",
          component: () => import("@/view/pages/ClientCardModule/CardsIndex")
        },
        { 
          name: "addClientCard",
          path: "/clientcards/add",
          component: () => import("@/view/pages/ClientCardModule/AddCard")
        },

        // Address Book Module
        { 
          name: "addressBook",
          path: "/addressbook/detail",
          component: () => import("@/view/pages/AddressBook/Index")
        },
        { 
          name: "AddressBookCreate",
          path: "/addressbook/create",
          component: () => import("@/view/pages/AddressBook/Create")
        },

        // Order Module
        { 
          name: "orders-history",
          path: "/orders/history",
          component: () => import("@/view/pages/Orders/orderHistory")
        },
        { 
          name: "orders-pending",
          path: "/orders/pending",
          component: () => import("@/view/pages/Orders/Index")
        },
        { 
          name: "create-order",
          path: "/orders/create",
          component: () => import("@/view/pages/Orders/Create")
        },
        { 
          name: "editOrder",
          path: "/orders/edit/:e_id",
          component: () => import("@/view/pages/Orders/Edit")
        },
        { 
          name: "viewOrder",
          path: "/orders/view/:id",
          component: () => import("@/view/pages/Orders/View")
        },
        { 
          name: "addItem",
          path: "/orders/addItem/:order_id",
          component: () => import("@/view/pages/Orders/AddItem")
        },
        { 
          name: "specificItemUpdate",
          path: "/orders/editItem/:order_id/:order_item_id",
          component: () => import("@/view/pages/Orders/UpdateSpecificItem")
        },
        { 
          name: "viewItem",
          path: "/orders/viewItem/:order_id",
          component: () => import("@/view/pages/Orders/Item")
        },
        // { 
        //   name: "updateItem",
        //   path: "/orders/updateItem/:order_id",
        //   component: () => import("@/view/pages/Orders/ItemUpdate")
        // },
        
        //Company Module
        {
          name: "company-detail",
          path: "/company/detail",
          component: () => import("@/view/pages/CompanyDetail/Detail")
        },
        {
          name: "company-payMethods",
          path: "/company/paymentMethods",
          component: () => import("@/view/pages/CompanyDetail/PaymentMethod")
        },
        { 
          name: "add-card",
          path: "/company/add-card",
          component: () => import("@/view/pages/CompanyDetail/AddCard")
        },
        { 
          name: "view-cards",
          path: "/company/view-cards",
          component: () => import("@/view/pages/CompanyDetail/ViewCards")
        },
        {
          name: "company-invoices",
          path: "/company/invoice",
          component: () => import("@/view/pages/CompanyDetail/Invoices")
        },
        {
          name: "company-users",
          path: "/company/users",
          component: () => import("@/view/pages/CompanyDetail/Users")
        },
        {
          name: "company-prefrences",
          path: "/company/prefrences",
          component: () => import("@/view/pages/CompanyDetail/Preferences")
        },
        
        // Users Module
        { 
          name: "userDetail",
          path: "/user/detail",
          component: () => import("@/view/pages/User/UsersDetail.vue")
        },
        { 
          name: "userInformation",
          path: "/user/information",
          component: () => import("@/view/pages/User/UsersInformation.vue")
        },
        { 
          name: "userLogs",
          path: "/user/logs",
          component: () => import("@/view/pages/User/UserLogs.vue")
        },

        // UnderConstruction
        {
          name: "underconstruction",
          path: "/underconstruction",
          component: () => import("@/view/pages/error/Error-5.vue")
        },
        
        {
          path: "/vue-bootstrap",
          name: "vue-bootstrap",
          component: () =>
            import("@/view/pages/vue-bootstrap/VueBootstrap.vue"),
          children: [
            {
              path: "alert",
              name: "vue-bootstrap-alert",
              component: () => import("@/view/pages/vue-bootstrap/Alert.vue")
            },
            {
              path: "badge",
              name: "vue-bootstrap-badge",
              component: () => import("@/view/pages/vue-bootstrap/Badge.vue")
            },
            {
              path: "button",
              name: "vue-bootstrap-button",
              component: () => import("@/view/pages/vue-bootstrap/Button.vue")
            },
            {
              path: "button-group",
              name: "vue-bootstrap-button-group",
              component: () =>
                import("@/view/pages/vue-bootstrap/ButtonGroup.vue")
            },
            {
              path: "button-toolbar",
              name: "vue-bootstrap-button-toolbar",
              component: () =>
                import("@/view/pages/vue-bootstrap/ButtonToolbar.vue")
            },
            {
              path: "card",
              name: "vue-bootstrap-card",
              component: () => import("@/view/pages/vue-bootstrap/Card.vue")
            },
            {
              path: "carousel",
              name: "vue-bootstrap-carousel",
              component: () => import("@/view/pages/vue-bootstrap/Carousel.vue")
            },
            {
              path: "collapse",
              name: "vue-bootstrap-collapse",
              component: () => import("@/view/pages/vue-bootstrap/Collapse.vue")
            },
            {
              path: "dropdown",
              name: "vue-bootstrap-dropdown",
              component: () => import("@/view/pages/vue-bootstrap/Dropdown.vue")
            },
            {
              path: "embed",
              name: "vue-bootstrap-embed",
              component: () => import("@/view/pages/vue-bootstrap/Embed.vue")
            },
            {
              path: "form",
              name: "vue-bootstrap-form",
              component: () => import("@/view/pages/vue-bootstrap/Form.vue")
            },
            {
              path: "form-checkbox",
              name: "vue-bootstrap-form-checkbox",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormCheckbox.vue")
            },
            {
              path: "form-file",
              name: "vue-bootstrap-form-file",
              component: () => import("@/view/pages/vue-bootstrap/FormFile.vue")
            },
            {
              path: "form-group",
              name: "vue-bootstrap-form-group",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormGroup.vue")
            },
            {
              path: "form-input",
              name: "vue-bootstrap-form-input",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormInput.vue")
            },
            {
              path: "form-radio",
              name: "vue-bootstrap-form-radio",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormRadio.vue")
            },
            {
              path: "form-select",
              name: "vue-bootstrap-form-select",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormSelect.vue")
            },
            {
              path: "form-textarea",
              name: "vue-bootstrap-form-textarea",
              component: () =>
                import("@/view/pages/vue-bootstrap/FormTextarea.vue")
            },
            {
              path: "image",
              name: "vue-bootstrap-image",
              component: () => import("@/view/pages/vue-bootstrap/Image.vue")
            },
            {
              path: "input-group",
              name: "vue-bootstrap-input-group",
              component: () =>
                import("@/view/pages/vue-bootstrap/InputGroup.vue")
            },
            {
              path: "jumbotron",
              name: "vue-bootstrap-jumbotron",
              component: () =>
                import("@/view/pages/vue-bootstrap/Jumbotron.vue")
            },
            {
              path: "layout-grid-system",
              name: "vue-bootstrap-layout-grid-system",
              component: () =>
                import("@/view/pages/vue-bootstrap/LayoutGridSystem.vue")
            },
            {
              path: "link",
              name: "vue-bootstrap-link",
              component: () => import("@/view/pages/vue-bootstrap/Link.vue")
            },
            {
              path: "list-group",
              name: "vue-bootstrap-list-group",
              component: () =>
                import("@/view/pages/vue-bootstrap/ListGroup.vue")
            },
            {
              path: "media",
              name: "vue-bootstrap-media",
              component: () => import("@/view/pages/vue-bootstrap/Media.vue")
            },
            {
              path: "modal",
              name: "vue-bootstrap-modal",
              component: () => import("@/view/pages/vue-bootstrap/Modal.vue")
            },
            {
              path: "nav",
              name: "vue-bootstrap-nav",
              component: () => import("@/view/pages/vue-bootstrap/Nav.vue")
            },
            {
              path: "navbar",
              name: "vue-bootstrap-navbar",
              component: () => import("@/view/pages/vue-bootstrap/Navbar.vue")
            },
            {
              path: "pagination",
              name: "vue-bootstrap-pagination",
              component: () =>
                import("@/view/pages/vue-bootstrap/Pagination.vue")
            },
            {
              path: "pagination-nav",
              name: "vue-bootstrap-pagination-nav",
              component: () =>
                import("@/view/pages/vue-bootstrap/PaginationNav.vue")
            },
            {
              path: "popover",
              name: "vue-bootstrap-popover",
              component: () => import("@/view/pages/vue-bootstrap/Popover.vue")
            },
            {
              path: "progress",
              name: "vue-bootstrap-progress",
              component: () => import("@/view/pages/vue-bootstrap/Progress.vue")
            },
            {
              path: "spinner",
              name: "vue-bootstrap-spinner",
              component: () => import("@/view/pages/vue-bootstrap/Spinner.vue")
            },
            {
              path: "table",
              name: "vue-bootstrap-table",
              component: () => import("@/view/pages/vue-bootstrap/Table.vue")
            },
            {
              path: "tabs",
              name: "vue-bootstrap-tabs",
              component: () => import("@/view/pages/vue-bootstrap/Tabs.vue")
            },
            {
              path: "toasts",
              name: "vue-bootstrap-toasts",
              component: () => import("@/view/pages/vue-bootstrap/Toasts.vue")
            },
            {
              path: "tooltip",
              name: "vue-bootstrap-tooltip",
              component: () => import("@/view/pages/vue-bootstrap/Tooltip.vue")
            }
          ]
        },
        {
          path: "/vuetify",
          name: "vuetify",
          component: () => import("@/view/pages/vuetify/Vuetify.vue"),
          children: [
            {
              path: "alerts",
              name: "vuetify-alerts",
              component: () => import("@/view/pages/vuetify/Alerts.vue")
            },
            {
              path: "avatars",
              name: "vuetify-avatars",
              component: () => import("@/view/pages/vuetify/Avatars.vue")
            },
            {
              path: "badges",
              name: "vuetify-badges",
              component: () => import("@/view/pages/vuetify/Badges.vue")
            },
            {
              path: "buttons",
              name: "vuetify-buttons",
              component: () => import("@/view/pages/vuetify/Buttons.vue")
            },
            {
              path: "calendars",
              name: "vuetify-calendars",
              component: () => import("@/view/pages/vuetify/Calendars.vue")
            },
            {
              path: "cards",
              name: "vuetify-cards",
              component: () => import("@/view/pages/vuetify/Cards.vue")
            },
            {
              path: "chips",
              name: "vuetify-chips",
              component: () => import("@/view/pages/vuetify/Chips.vue")
            },
            {
              path: "dialog",
              name: "vuetify-dialog",
              component: () => import("@/view/pages/vuetify/Dialog.vue")
            },
            {
              path: "autocompletes",
              name: "vuetify-autocompletes",
              component: () =>
                import("@/view/pages/vuetify/forms/Autocompletes.vue")
            },
            {
              path: "file-inputs",
              name: "vuetify-file-inputs",
              component: () =>
                import("@/view/pages/vuetify/forms/FileInputs.vue")
            },
            {
              path: "forms",
              name: "vuetify-forms",
              component: () => import("@/view/pages/vuetify/forms/Forms.vue")
            },
            {
              path: "selection-controls",
              name: "vuetify-selection-controls",
              component: () =>
                import("@/view/pages/vuetify/forms/SelectionControls.vue")
            },
            {
              path: "selects",
              name: "vuetify-selects",
              component: () => import("@/view/pages/vuetify/forms/Selects.vue")
            },
            {
              path: "textareas",
              name: "vuetify-textareas",
              component: () =>
                import("@/view/pages/vuetify/forms/Textareas.vue")
            },
            {
              path: "text-fields",
              name: "vuetify-text-fields",
              component: () =>
                import("@/view/pages/vuetify/forms/TextFields.vue")
            },
            {
              path: "simple-tables",
              name: "vuetify-simple-tables",
              component: () =>
                import("@/view/pages/vuetify/tables/SimpleTables.vue")
            },
            {
              path: "data-tables",
              name: "vuetify-data-tables",
              component: () =>
                import("@/view/pages/vuetify/tables/DataTables.vue")
            },
            {
              path: "tabs",
              name: "vuetify-tabs",
              component: () => import("@/view/pages/vuetify/Tabs.vue")
            },
            {
              path: "timelines",
              name: "vuetify-timelines",
              component: () => import("@/view/pages/vuetify/Timelines.vue")
            },
            {
              path: "tooltips",
              name: "vuetify-tooltips",
              component: () => import("@/view/pages/vuetify/Tooltips.vue")
            },
            {
              path: "treeview",
              name: "vuetify-treeview",
              component: () => import("@/view/pages/vuetify/Treeview.vue")
            }
          ]
        },
        {
          path: "/wizard",
          name: "wizard",
          component: () => import("@/view/pages/wizard/Wizard.vue"),
          children: [
            {
              path: "wizard-1",
              name: "wizard-1",
              component: () => import("@/view/pages/wizard/Wizard-1.vue")
            },
            {
              path: "wizard-2",
              name: "wizard-2",
              component: () => import("@/view/pages/wizard/Wizard-2.vue")
            },
            {
              path: "wizard-3",
              name: "wizard-3",
              component: () => import("@/view/pages/wizard/Wizard-3.vue")
            },
            {
              path: "wizard-4",
              name: "wizard-4",
              component: () => import("@/view/pages/wizard/Wizard-4.vue")
            }
          ]
        }
      ]
    },

    {
      path: "/error",
      name: "error",
      component: () => import("@/view/pages/error/Error.vue"),
      children: [
        {
          path: "error-1",
          name: "error-1",
          component: () => import("@/view/pages/error/Error-1.vue")
        },
        {
          path: "error-2",
          name: "error-2",
          component: () => import("@/view/pages/error/Error-2.vue")
        },
        {
          path: "error-3",
          name: "error-3",
          component: () => import("@/view/pages/error/Error-3.vue")
        },
        {
          path: "error-4",
          name: "error-4",
          component: () => import("@/view/pages/error/Error-4.vue")
        },
        {
          path: "error-5",
          name: "error-5",
          component: () => import("@/view/pages/error/Error-5.vue")
        },
        {
          path: "error-6",
          name: "error-6",
          component: () => import("@/view/pages/error/Error-6.vue")
        }
      ]
    },
    {
      path: "/",
      component: () => import("@/view/pages/auth/Auth"),
      children: [
        {
          name: "login",
          path: "/login",
          component: () => import("@/view/pages/auth/Login")
        },
        {
          name: "register",
          path: "/register",
          component: () => import("@/view/pages/auth/Register")
        },
        {
          name: "client-register",
          path: "/client/register",
          component: () => import("@/view/pages/auth/clientRegister")
        },
        {
          name: "forgotpassword",
          path: "/forgotpassword",
          component: () => import("@/view/pages/auth/ForgotPassword")
        },
        { 
          name: "resetpassword",
          path: "/resetpassword",
          component: () => import("@/view/pages/auth/ResetPassword")
          },
        
        {
          name: "thankyou",
          path: "/thankyou",
          component: () => import("@/view/pages/auth/ThankYou")
        },
      ]
    },
    {
      path: "*",
      redirect: "/404"
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/error/Error-1.vue")
    }
  ]
});
