import Vue from "vue";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./core/services/store";
// import ApiService from "./core/services/api.service";
import MockService from "./core/mock/mock.service";
import { VERIFY_AUTH } from "./core/services/store/auth.module";
import { RESET_LAYOUT_CONFIG } from "@/core/services/store/config.module";

import VueLodash from 'vue-lodash'
import lodash from 'lodash'


// name is optional
Vue.use(VueLodash, { name: 'custom', lodash: lodash })

Vue.config.productionTip = false;

// Global 3rd party plugins
import "popper.js";
import "tooltip.js";
import PerfectScrollbar from "perfect-scrollbar";
window.PerfectScrollbar = PerfectScrollbar;
import ClipboardJS from "clipboard";
window.ClipboardJS = ClipboardJS;

// Vue 3rd party plugins
import i18n from "./core/plugins/vue-i18n";
import vuetify from "./core/plugins/vuetify";
import "./core/plugins/portal-vue";
import "./core/plugins/bootstrap-vue";
import "./core/plugins/perfect-scrollbar";
import "./core/plugins/highlight-js";
import "./core/plugins/inline-svg";
import "./core/plugins/apexcharts";
import "./core/plugins/metronic";
import "@mdi/font/css/materialdesignicons.css";




import { LOGOUT } from "@/core/services/store/auth.module";


// API service init
// ApiService.init();
axios.defaults["baseURL"] = "https://couriersx.herokuapp.com/";

Vue.prototype.$axios = axios;

// Remove this to disable mock API
MockService.init();

router.beforeEach((to, from, next) => {
  // reset config to initial state
  store.dispatch(RESET_LAYOUT_CONFIG);

  // Ensure we checked auth before each page load.
  Promise.all([store.dispatch(VERIFY_AUTH)]).then(next);



  let now = new Date();
  let user = store.state.auth.user;
  var dif = now.getTime() - new Date(user.created_at).getTime();
  dif = dif / 1000;
  dif = Math.abs(dif);
  dif = dif * 1000;
  if (dif > user.exp) {

    axios({
      method: "post",
      url: "user/refreshToken",
      headers: {
        'Authorization': `Bearer ${store.state.auth.user.access_token}`
      },
      data: {
        refresh_token: store.state.auth.user.token
      },
    })
      .then(response => {
        // console.log(response.data);
        var user = parseJwt(response.data.access_token);
        user.created_at = new Date();
        user.access_token = response.data.access_token;
        user.token = response.data.refresh_token;
        store.commit("setUser", user);
        router.push({ name: "dashboard" });
      })
      .catch(err => {
        // console.log(err.response);
        if (err) {
          store.dispatch(LOGOUT);
          router.push({ name: "login" });
        }
      })
      .finally(() => {
        // console.log("finally");
      });
  }

  function parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsoPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );
    return JSON.parse(jsoPayload);
  }

  // Scroll page to top on every route change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount("#app");

