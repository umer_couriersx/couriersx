import ApiService from "@/core/services/api.service";
import JwtService from "@/core/services/jwt.service";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";


// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_ERROR = "setError";
export const UPDATE = "updateUser";
export const RESET_PASSWORD = "resetpassword";
export const CLIENTS = "updateClient";
export const Set_CLIENT = "setClient";
export const NEW_CONSIGNMENT = "setNewConsignment";
export const CARRIER_ID = "setNewCarrierId";
export const STATS = "setStats";


// export const ORDERS = "orders";

const state = {
  errors: null,
  user: {},
  newConsignment: {},
  newCarrierID: '',
  stats: null,
  isAuthenticated: !!JwtService.getToken()
};

export const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
  getNewConsignment(state) {
    return state.newConsignment;
  },
  getCarrierId(state) {
    return state.newCarrierID;
  },
  getStats(state) {
    return state.stats;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.post("login", credentials)
        .then(({ data }) => {
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },

  [REGISTER](context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.post("users", { user: credentials })
        .then(({ data }) => {
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
          reject(response);
        });
    });
  },

  [VERIFY_AUTH](context) {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      context.commit(SET_AUTH, user);
    } else {
      context.commit(PURGE_AUTH);
    }
  },

  [UPDATE_USER](context, payload) {
    const { email, username, password, image, bio } = payload;
    const user = { email, username, bio, image };
    if (password) {
      user.password = password;
    }

    return ApiService.put("user", user).then(({ data }) => {
      context.commit(SET_AUTH, data);
      return data;
    });
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },

  [STATS](state, value) {
    state.stats = value;
  },

  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    localStorage.setItem('user', JSON.stringify(user));
  },

  [CLIENTS](state, clients) {
    state.isAuthenticated = true;
    state.user.clients = [];
    state.user.clients = clients;
    localStorage.setItem('user', JSON.stringify(state.user));
  },

  [Set_CLIENT](state, setclient) {
    state.isAuthenticated = true;
    state.user.setClient_accessToken = setclient.access_token;
    state.user.setClient_client_name = setclient.clientName;
    state.user.setClient_client_id = setclient.clientId;
    localStorage.setItem('user', JSON.stringify(state.user));
  },

  [UPDATE](state, user) {
    state.isAuthenticated = true;
    state.user.email = user.email;
    state.user.full_name = user.full_name;
    localStorage.setItem('user', JSON.stringify(state.user));
  },

  [RESET_PASSWORD](state, resetPassword) {
    state.isAuthenticated = false;
    state.resetPassword = resetPassword;
    localStorage.setItem('unique_code', JSON.stringify(resetPassword));
  },

  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    localStorage.removeItem('user');
  },

  [NEW_CONSIGNMENT](state, value) {
    state.isAuthenticated = true;
    state.newConsignment = value;
  },

  [CARRIER_ID](state, value) {
    state.isAuthenticated = true;
    state.newCarrierID = value;
  }


};

export default {
  state,
  actions,
  mutations,
  getters
};
